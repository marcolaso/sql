
SELECT jstat_gc.instance,
       CASE WHEN max(jstat_gc.s0c) =  min(jstat_gc.s0c)
         THEN '' 
         ELSE max(jstat_gc.s0c) ||'  ' || min(jstat_gc.s0c)  
       END as s0c,
       CASE WHEN max(jstat_gc.s1c) =  min(jstat_gc.s1c)
         THEN ''
         ELSE max(jstat_gc.s1c) ||'  ' || min(jstat_gc.s1c)
       END as s1c
FROM memory_jvm.jstat_gc,
     memory_jvm.jstat_gccapacity
WHERE jstat_gc.date     = jstat_gccapacity.date
  AND jstat_gc.instance = jstat_gccapacity.instance
  AND jstat_gc.instance LIKE 'epr%'
-- ORDER BY jstat_gc.instance
--         jstat_gc.date 
GROUP BY jstat_gc.instance ;
