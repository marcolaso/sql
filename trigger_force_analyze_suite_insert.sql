
CREATE FUNCTION analyze_table() 
RETURNS TRIGGER SECURITY DEFINER AS $$ 
BEGIN
ANALYZE  schema.table;       
RETURN NULL;                                                                                                                          
END;                                                                                                                                  
$$  language plpgsql;

CREATE TRIGGER analyze_table 
AFTER INSERT ON schema.table 
FOR EACH STATEMENT EXECUTE FUNCTION analyze_table();

