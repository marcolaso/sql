


CREATE OR REPLACE PROCEDURE create_user_test(trigram varchar (10) , nom varchar(10), prenom varchar (10) ) 
    LANGUAGE plpgsql    
AS $PLSQL$
DECLARE
    fiscalite_engagement_id_seq    integer ;
    partenaire_id_seq              integer ;
    contact_id_seq                 integer ;
    adresse_id_seq                 integer ;
    correspondance_id_seq          integer ;
    version			   integer ;
BEGIN 
    IF (SELECT count(visa) FROM partner.correspondance WHERE visa = lower(trigram) ) = 0  
    THEN
    	SELECT nextval('partner.fiscalite_engagement_id_seq')   INTO fiscalite_engagement_id_seq;
    	SELECT nextval('partner.partenaire_id_seq')             INTO partenaire_id_seq;
       	SELECT nextval('partner.correspondance_id_seq')         INTO correspondance_id_seq ;
      	SELECT nextval('partner.contact_id_seq')                INTO contact_id_seq;
      	SELECT nextval('partner.adresse_id_seq')                INTO adresse_id_seq;

	SELECT SUBSTR(id,0,3) from partner.databasechangelog ORDER BY orderexecuted DESC LIMIT 1 INTO version ; 
			
	IF version = 19
	THEN
		INSERT INTO partner.partenaire (id, fiscalite_engagement_id, nom, etat_generation, statut, numero, designation_complementaire, apporteur_affaire, cause_radiation, classe, code_succession, langue, numero_agent, numero_immobilier, statut_agent, version, usr_log_i, dte_log_i, usr_log_u, dte_log_u, type_client_vip, division_responsable, type_partenaire_marketing, canal_diffusion)
			VALUES (partenaire_id_seq, fiscalite_engagement_id_seq, nom, 'ACTIF', 'VALIDE', nextval('partner.partenaire_numero_seq'), null, null, null, null, null, 'FR', null, null, null,  0, 'fpo', now(), 'fpo', now(), null, null, 'PARTENAIRE', 'PAPIER');
		INSERT INTO partner.contact_adresse (id, contact_id, adresse_id, complement_destinataire_1, complement_destinataire_2, confidentialite, numero, sexe_complement_destinataire, type_adresse_list, version)
			VALUES (nextval('partner.contact_adresse_id_seq'), contact_id_seq, adresse_id_seq, null, null, null, nextval('partner.contact_adresse_numero_seq'), null, 'BASE', 0);
	END IF;
	IF version >= 20
	THEN
		INSERT INTO partner.partenaire (id, fiscalite_engagement_id, nom, etat_generation, statut, numero, designation_complementaire, apporteur_affaire, cause_radiation, classe, code_succession, langue, numero_agent, numero_immobilier, statut_agent, version, usr_log_i, dte_log_i, usr_log_u, dte_log_u, type_client_vip, division_responsable, type_partenaire_marketing, canal_diffusion, succession_repudiee)
			VALUES (partenaire_id_seq, fiscalite_engagement_id_seq, nom, 'ACTIF', 'VALIDE', nextval('partner.partenaire_numero_seq'), null, null, null, null, null, 'FR', null, null, null,  0, 'fpo', now(), 'fpo', now(), null, null, 'PARTENAIRE', 'PAPIER', false);
		INSERT INTO partner.contact_adresse (id, contact_id, adresse_id, complement_destinataire_1, complement_destinataire_2, confidentialite, numero, sexe_complement_destinataire, type_adresse_list, version, date_debut, date_fin)
			VALUES (nextval('partner.contact_adresse_id_seq'), contact_id_seq, adresse_id_seq, null, null, null, nextval('partner.contact_adresse_numero_seq'), null, 'BASE', 0, '01.01.1970', '31.12.9999');
	END IF;
    else
	Raise notice 'user % existe deja !' ,trigram ;
    END IF;
END;
$PLSQL$; 

call create_user_test  ('TO1','Tosca','1');
call create_user_test  ('TU1','Test','User1');
